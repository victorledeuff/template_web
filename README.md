[![pipeline status](https://git.energiesdemain.org/ed---interne/template_web/badges/main/pipeline.svg)](https://git.energiesdemain.org/ed---interne/template_web/-/commits/main)
[![coverage report](https://git.energiesdemain.org/ed---interne/template_web/badges/main/coverage.svg)](https://git.energiesdemain.org/ed---interne/template_web/-/commits/main)


## Description

Ce projet constitue le boilerplate de tous nos prochains projets Web.

Il est constitué :
- D'un back NestJS : voir le README dans le dossier back/
- D'un front Angular : voir le README dans le dossier front/
- un devops tout fait : politique de tests et déploiement auto sur la branche main.


Avant de reprendre le projet tel quel, il sera toujours opportuns de remettre en question les choix technologiques, ainsi que les versions de ces derniers.

## Lancer le code en local

D'abord, il faut configurer le fichier .env du dossier back (voir README.md du dossier back). Celui ci est utilisé par docker pour initialiser la base de données, et permettre au back de s'y connecter.

Ensuite, Il suffit d'executer la commande :

```bash
docker-compose up -d
```

Docker lancera 3 containers: 
- Une base de données
- Un back en mode debug ( Avec Hot reloading, et les points d'arrêts fonctionnent avec VSCode)
- Un front (TODO)

Vous avez aussi la possibilité de lancer le code directement depuis votre PC (voir README.md dans les dossiers back et front)