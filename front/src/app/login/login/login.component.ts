import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent {
    email = '';
    password = '';

    constructor(private router: Router, @Inject(AuthService) private authService: AuthService) {}

    async login() {
        try {
            await this.authService.login(this.email, this.password);
            this.router.navigate(['main']);
        } catch (error) {
            console.log(error);
        }
    }
}
