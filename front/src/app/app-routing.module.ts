import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { canActivate } from './core/services/auth.guard';
import { LoginModule } from './login/login.module';
import { LoginComponent } from './login/login/login.component';
import { MainModule } from './main/main.module';
import { MainComponent } from './main/main/main.component';

const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'main', component: MainComponent, canActivate: [canActivate] },
];

@NgModule({
    imports: [RouterModule.forRoot(routes), LoginModule, MainModule],
    exports: [RouterModule],
})
export class AppRoutingModule {}
