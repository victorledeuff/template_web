import { Inject, Injectable } from '@angular/core';
import { RestService } from './rest.service';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(@Inject(RestService) private restService: RestService) {}

    async login(email: string, password: string) {
        const path = '/auth/login';
        const data = {
            mail: email,
            password: password,
        };
        try {
            const result = await this.restService.post(path, {}, data);
            if (result.access_token) {
                localStorage.setItem('Authorization', 'bearer ' + result.access_token);
            } else {
                localStorage.removeItem('Authorization');
            }
            return result;
        } catch (error) {
            throw error;
        }
    }

    isAuthenticated(): boolean {
        const token = localStorage.getItem('Authorization');
        return !!token && token.includes('bearer ');
    }
}
