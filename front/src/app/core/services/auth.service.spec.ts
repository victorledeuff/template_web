import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { AuthService } from './auth.service';

describe('AuthService', () => {
    let authService: AuthService;
    let restServiceMock: any;

    beforeEach(() => {
        localStorage.clear();
        restServiceMock = {
            post: jest.fn(),
        };
        authService = new AuthService(restServiceMock);
    });

    it('should be created', () => {
        expect(authService).toBeTruthy();
    });

    it('should not be authenticated without login', () => {
        const isAuthenticated = authService.isAuthenticated();
        expect(isAuthenticated).not.toBeTruthy();
        expect(localStorage['__STORE__']['Authorization']).not.toBeTruthy();
    });

    it('should login and save token in local storage', async () => {
        const token = 'token';
        const expectedResult = {
            access_token: token,
        };
        jest.spyOn(restServiceMock, 'post').mockResolvedValue(expectedResult);
        const result = await authService.login('mail', 'password');
        expect(restServiceMock.post).toBeCalledTimes(1);
        expect(restServiceMock.post).toHaveBeenCalledWith(
            '/auth/login',
            {},
            {
                mail: 'mail',
                password: 'password',
            },
        );
        expect(result).toEqual(expectedResult);
        expect(localStorage.setItem).toHaveBeenLastCalledWith('Authorization', `bearer ${token}`);
        expect(localStorage['__STORE__']['Authorization']).toBe(`bearer ${token}`);
        expect(Object.keys(localStorage['__STORE__']).length).toBe(1);
    });

    it('should be authenticated after login', async () => {
        const token = 'token';
        const expectedResult = {
            access_token: token,
        };
        jest.spyOn(restServiceMock, 'post').mockResolvedValue(expectedResult);
        const result = await authService.login('mail', 'password');

        const isAuthenticated = authService.isAuthenticated();
        expect(isAuthenticated).toBeTruthy();
        expect(localStorage['__STORE__']['Authorization']).toBeTruthy();
    });

    it('should not login nor save token in local storage when error', async () => {
        const errorResponse = new HttpErrorResponse({
            status: 401,
            statusText: 'Unauthorized',
        });
        jest.spyOn(restServiceMock, 'post').mockRejectedValue(throwError(() => errorResponse));

        try {
            await authService.login('mail', 'password');
        } catch (error: any) {
            expect(Object.keys(localStorage['__STORE__']).length).toBe(0);
        }
    });

    it('should not login nor save token in local storage when no token is received', async () => {
        const wrongResult = {
            foo: 'bar',
        };
        jest.spyOn(restServiceMock, 'post').mockResolvedValue(wrongResult);

        const result = await authService.login('mail', 'password');
        expect(restServiceMock.post).toBeCalledTimes(1);
        expect(restServiceMock.post).toHaveBeenCalledWith(
            '/auth/login',
            {},
            {
                mail: 'mail',
                password: 'password',
            },
        );
        expect(result).toEqual(wrongResult);
        expect(Object.keys(localStorage['__STORE__']).length).toBe(0);
    });
});
