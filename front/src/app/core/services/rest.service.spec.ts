import { HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { RestService } from './rest.service';

describe('RestService', () => {
    let restService: RestService;
    let httpClientMock: any;
    const apiUrl = environment.apiUrl;

    beforeEach(() => {
        httpClientMock = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
            delete: jest.fn(),
        };
        restService = new RestService(httpClientMock);
    });

    it('should be created', () => {
        expect(restService).toBeTruthy();
    });

    it('should get url', async () => {
        const path = '/path/to';
        const url = apiUrl + path;
        const expectedResult = 'result';
        jest.spyOn(httpClientMock, 'get').mockReturnValue(of(expectedResult));
        const result = await restService.get(path, {});
        expect(httpClientMock.get).toBeCalledTimes(1);
        expect(httpClientMock.get).toHaveBeenCalledWith(url);
        expect(result).toEqual(expectedResult);
    });

    it('should call delete url', async () => {
        const path = '/path/to';
        const url = apiUrl + path;
        const expectedResult = 'result';
        jest.spyOn(httpClientMock, 'delete').mockReturnValue(of(expectedResult));
        const result = await restService.delete(path, {});
        expect(httpClientMock.delete).toBeCalledTimes(1);
        expect(httpClientMock.delete).toHaveBeenCalledWith(url);
        expect(result).toEqual(expectedResult);
    });

    it('should get url with params', async () => {
        const path = '/path/to';
        const params = {
            foo: 'bar',
            hello: 'world',
        };
        const url = apiUrl + path + '?foo=bar&hello=world';
        const expectedResult = 'result';
        jest.spyOn(httpClientMock, 'get').mockReturnValue(of(expectedResult));
        const result = await restService.get(path, params);
        expect(httpClientMock.get).toBeCalledTimes(1);
        expect(httpClientMock.get).toHaveBeenCalledWith(url);
        expect(result).toEqual(expectedResult);
    });

    it('should post url with data', async () => {
        const path = '/path/to';
        const url = apiUrl + path;
        const data = {
            foo: 'bar',
            value: 42,
        };
        const expectedResult = 'result';
        jest.spyOn(httpClientMock, 'post').mockReturnValue(of(expectedResult));
        const result = await restService.post(path, {}, data);
        expect(httpClientMock.post).toBeCalledTimes(1);
        expect(httpClientMock.post).toHaveBeenCalledWith(url, data);
        expect(result).toEqual(expectedResult);
    });

    it('should put url with data', async () => {
        const path = '/path/to';
        const url = apiUrl + path;
        const data = {
            foo: 'bar',
            value: 42,
        };
        const expectedResult = 'result';
        jest.spyOn(httpClientMock, 'put').mockReturnValue(of(expectedResult));
        const result = await restService.put(path, {}, data);
        expect(httpClientMock.put).toBeCalledTimes(1);
        expect(httpClientMock.put).toHaveBeenCalledWith(url, data);
        expect(result).toEqual(expectedResult);
    });

    it('should throw error', async () => {
        const path = '/path/to';
        const url = apiUrl + path;
        const errorResponse = new HttpErrorResponse({
            status: 404,
            statusText: 'test 404 error',
        });
        jest.spyOn(httpClientMock, 'get').mockReturnValue(throwError(() => errorResponse));
        try {
            const result = await restService.get(path, {});
        } catch (error: any) {
            expect(httpClientMock.get).toBeCalledTimes(1);
            expect(httpClientMock.get).toHaveBeenCalledWith(url);
            expect(error.message).toContain('test 404 error');
        }
    });
});
