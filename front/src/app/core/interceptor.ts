import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');
        const isApiRequest = httpRequest.url.includes(environment.apiUrl);
        if (token && isApiRequest) {
            httpRequest = httpRequest.clone({
                setHeaders: {
                    'x-auth-token': token,
                },
            });
        }
        return next.handle(httpRequest);
    }
}
