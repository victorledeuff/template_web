import { Component, Input } from '@angular/core';

@Component({
  selector: 'edButton',
  templateUrl: './ed-button.component.html',
  styleUrls: ['./ed-button.component.sass']
})

export class EdButtonComponent {
  @Input() label = '';
  @Input() type: 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'light' = 'primary';
  @Input() icon = '';
  @Input() iconPosition: 'left' | 'right' = 'left';
  @Input() isDisabled = false;
}
