import { EdButtonComponent } from './ed-button.component';
import { Meta, Story } from '@storybook/angular';

export default {
    title: 'Forms/Button',
    component: EdButtonComponent,
} as Meta;

const Template: Story<any> = (args) => {
    args.onClick = () => {
        alert('clicked');
    };

    return {
        props: args,
    };
};

export const Primary = Template.bind({});
Primary.args = {
    type: 'primary',
    label: 'Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
    type: 'secondary',
    label: 'Button',
};

export const Disabled = Template.bind({});
Disabled.args = {
    type: 'primary',
    label: 'Button',
    isDisabled: true,
};

export const IconLeft = Template.bind({});
IconLeft.args = {
    type: 'primary',
    label: 'Button',
    icon: 'bi-save',
};
