import { EdInputComponent } from './ed-input.component';
import { Meta, Story } from '@storybook/angular';

export default {
    title: 'Forms/Input',
    component: EdInputComponent,
} as Meta;

const Template: Story<any> = (args) => {
    args.onClick = () => {
        alert('clicked');
    };

    return {
        props: args,
    };
};

export const Text = Template.bind({});
Text.args = {
    type: 'text',
    placeholder: 'Text input',
};

export const Number = Template.bind({});
Number.args = {
    type: 'number',
    min: 0,
    max: 5,
};

export const Password = Template.bind({});
Password.args = {
    type: 'password',
};

export const Disabled = Template.bind({});
Disabled.args = {
    type: 'text',
    isDisabled: true,
};

export const Invalid = Template.bind({});
Invalid.args = {
    type: 'text',
    isInvalid: true,
    invalidMessage: 'Champ non valide !',
};
