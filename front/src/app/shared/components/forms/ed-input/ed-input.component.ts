('use strict');

import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'edInput',
    templateUrl: './ed-input.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EdInputComponent),
            multi: true,
        },
    ],
})
export class EdInputComponent implements ControlValueAccessor {
    @Input() name = '';
    @Input() id = '';
    @Input() type: 'text' | 'number' | 'password' | 'email' = 'text';
    @Input() class = '';
    @Input() style = '';
    @Input() placeholder = '';
    @Input() autocomplete = '';
    @Input() invalidMessage = '';
    @Input() min = 0;
    @Input() max = 100;
    @Input() isRequired: boolean = false;
    @Input() isDisabled: boolean = false;
    @Input() isInvalid: boolean = false;
    @Output() ngBlur = new EventEmitter();

    constructor() {}
    onChange: any = () => {};
    onTouch: any = () => {};
    val = '';

    onBlur() {
        this.ngBlur.emit();
    }

    set value(value: string) {
        if (value !== undefined && this.val !== value) {
            this.val = value;
            this.onChange(value);
            this.onTouch(value);
        }
    }

    writeValue(value: string) {
        this.value = value;
    }

    registerOnChange(fn: any) {
        this.onChange = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouch = fn;
    }

    get aggregatedClass(): string {
        let _class = 'form-control';
        const isInvalid = this.isInvalid || (this.isRequired && !this.val);
        if (isInvalid) _class += ' is-invalid';
        if (this.class) _class += ` ${this.class}`;
        return _class;
    }
}
