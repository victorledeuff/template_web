import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EdInputComponent } from './components/forms/ed-input/ed-input.component';
import { FormsModule } from '@angular/forms';
import { EdButtonComponent } from './components/forms/ed-button/ed-button.component';

@NgModule({
    declarations: [EdInputComponent, EdButtonComponent],
    imports: [CommonModule, FormsModule],
    exports: [EdInputComponent, EdButtonComponent],
})
export class SharedModule {}
