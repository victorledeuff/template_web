<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">NestJs : A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>


[![pipeline status](https://git.energiesdemain.org/ed---interne/template_web/badges/main/pipeline.svg)](https://git.energiesdemain.org/ed---interne/template_web/-/commits/main)
[![coverage report](https://git.energiesdemain.org/ed---interne/template_web/badges/main/coverage.svg)](https://git.energiesdemain.org/ed---interne/template_web/-/commits/main)


<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>

<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>


## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

- 📱 NestJS — latest version
- 🎉 TypeScript - Type checking
- ⚙️ Dotenv - Supports environment variables (.env file)
- 🗝 Authentication - JWT
- 🏬 Authorization - PassportJs
- 🏪 TypeORM - Database ORM
- 🏪 PostgreSQL - Open-Source Relational Database
- 📃 Swagger - API Documentation (http://localhost:3000/docs)
- 🐳 Docker Compose - Container Orchestration
- 📏 ESLint — Pluggable JavaScript linter
- 💖 TODO Prettier - Opinionated Code Formatter

## Installation

```bash
$ npm install
```

## Running the app

First you have to setup the .env file: 
```bash
POSTGRES_USER=postgres
POSTGRES_PASSWORD=mysecretpassword
POSTGRES_DB=myDatabase
POSTGRES_HOST=db
POSTGRES_PORT=5432
#MODE=TEST with launch an inmemory DB. DEV or PRODUCTION will try to connect to database above
MODE=DEV
JWT_SECRET=secretKeysiAsecretkey
#Version de l'image à charger pour déploiement (prod uniquement)
IMAGE_VERSION=main
```


```bash
# development
$ npm run start

# watch mode (Mode Dev si vous ne souhaitez pas passer par docker)
$ npm run start:dev
#=> Si vous n'avez pas de base de données sous la main, vous pouvez saisir MODE=TEST dans le .env : cela lancera une base de donnée "in mémory" qui sera détruite à chaque lancement de l'application

# production mode
$ npm run start:prod
```

## CLI CRUD (Ou comment créer une nouvelle fonctionnalité...)

NestJs propose un CLI pour créer automatiquement vos fichiers de codes, avec un fichier test etc...

Par exemple, pour créer un nouveau module :
```bash
$ cd src/app && nest generate module todo
```
Créer le controller associé : 
```bash
$ cd src/app && nest generate controller todo
#Genere le controller, les tests sur le controller et l'ajoute au module
```
Créer le service associé : 
```bash
$ cd src/app && nest generate service todo
#Genere le service, les tests sur le service et l'ajoute au module
```

## TypeOrm
Ce projet utilise TypeOrm pour l'interfaçage avec la base de données.
La base de données est assurée par les classes *.entity.ts, et les requêtes SQL sont générées automatiquement par les repositories

## Migration
à chaque lancement du back-end, le projet vérifie qu'il ne doit pas lancer des migrations sur la base de données
```bash
# Create Migration (compare your local database and class and create a file in src/database/migrations, don't hesitate to modify it, to avoid data loss)
$ npm run typeorm:generate-migration


# Launch migration (si nécéssaire)
$ npm run typeorm:run-migrations

```

## Authentification

L'authentification se fait via un token JWT
Les routes marquées @UseGuards(JwtAuthGuard) sont protégés par ce token
Il faut utiliser la route GET /auth/login pour récupérer le token,
puis le passer dabs le heater "Authorization": "Bearer TOKEN" pour utiliser les routes protégées.

Une route est protégée si elle (ou son controller) est annontée   @UseGuards(JwtAuthGuard)


## Documentation

La documentation est assurée par swagger et accéssible via l'url /docs
Les décorateurs @ApiOperation (pour les routes), et @ApiProperty (Pour les entitiés) servent à documenter les usages

Les routes protégées par authentifications doivent avoir la mention @ApiBearerAuth()

## Test

Tous les services et les controllers sont à tester !

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).


## License

Nest is [MIT licensed](LICENSE).
