import { DataType, newDb } from "pg-mem";
import { v4 } from 'uuid';



export const setupDB=async (isE2ETest=false)=> {
    //Create in memory database
    const db = newDb();

    // Register current_database function (as it's used by Typeorm to synchronise entities)
    db.public.registerFunction({
        name: 'current_database',
        args: [],
        returns: DataType.text,
        implementation: (x) => `hello world ${x}`,
    });
    // Register version function (as it's used by Typeorm to synchronise entities)
    db.public.registerFunction({
        name: 'version',
        args: [],
        returns: DataType.text,
        implementation: () => `PostgreSQL 15.2 on x86_64-pc-linux-gnu (Debian 9.6.24-1.pgdg90+1), compiled by gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516, 64-bit`,
    });

    db.registerExtension('uuid-ossp', (schema) => {
        schema.registerFunction({
        name: 'uuid_generate_v4',
        returns: DataType.uuid,
        implementation: v4,
        impure: true,
        });
    });
        // Get PG in memory DB Datasource
    const dataSource = await db.adapters.createTypeormConnection({
        type: 'postgres',
        synchronize: false,
        migrationsRun: true,
        migrationsTableName: 'migration',
        entities: [isE2ETest ?'../**/*.entity{.ts,.js}' : 'dist/**/*.entity.js'],
        migrations: [isE2ETest ? 'dist/src/database/migrations/*.js' :'dist/src/database/migrations/*{.ts,.js}'],
        cli: {
        migrationsDir: './migration'
        },
        autoLoadEntities: true,
    });

    return {db, dataSource};
}