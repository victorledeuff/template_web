import { Controller, Get, Query, HttpException, HttpStatus, Body, Post, UseGuards, Logger, HttpCode } from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { User } from '../users/entities/user.entity';
import { JwtAuthGuard } from './jwt-auth.guard';


@ApiTags('auth')
@Controller('/auth')
export class AuthController {
  
  private readonly logger = new Logger(AuthController.name);
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Login user and return Token' })
  @Post('login')
  @HttpCode(200)
  async login(@Body('mail') mail: string, @Body('password') password: string,) {
    if(!mail) {
      throw new HttpException('Unauthorized: mail is required', HttpStatus.UNAUTHORIZED);
    }
    if(!password) {
      throw new HttpException('Unauthorized: password is required', HttpStatus.UNAUTHORIZED);
    }
    
    const user = await this.authService.validateUser(mail, password);
    if(user) {
      return this.authService.login(user);
    }
    else {
      throw new HttpException('Unauthorized: Mail and Password Missmatch', HttpStatus.UNAUTHORIZED);
    }
  }


  @ApiBearerAuth()
  @ApiOperation({ summary: "Création d'un nouvel utilisateur" })
  @UseGuards(JwtAuthGuard)
  @Post('user')
  async postNewUser(@Body() user: Partial<User>){

    if(!user || !user.mail){
      throw new HttpException('Unauthorized: mail is required', HttpStatus.UNAUTHORIZED);
    }
    if(!user || !user.password){
      throw new HttpException('Unauthorized: password is required', HttpStatus.UNAUTHORIZED);
    }

    return await this.authService.postNewUser(user);
  }
}
