
import { Test, TestingModule } from "@nestjs/testing";
import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { UsersService } from "../../users/users.service";
import { AuthService } from "../auth.service";
import { HASH_DEFAULT, MAIL_DEFAULT, PASSWORD_DEFAULT } from "../../../database/seed/seeds";
import { User } from "../../users/entities/user.entity";

describe('AuthService', () => {
    let authService: AuthService;
    let usersService: DeepMocked<UsersService>;
  
    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AuthService],
          }).useMocker((createMock)).compile();
      
          authService = module.get<AuthService>(AuthService);
          usersService= module.get(UsersService);
    });
  
    it('should be defined', () => {
      expect(authService).toBeDefined();
    });
    it('should validate user', async() => {
      const user: User= {id: "", mail: MAIL_DEFAULT, password: HASH_DEFAULT};
      usersService.findOne.mockResolvedValueOnce(user);
      const result = await authService.validateUser(MAIL_DEFAULT, PASSWORD_DEFAULT);
      expect(result).toEqual({id: user.id, mail: user.mail});
    });
    it('should NOT validate user (no user)', async() => {
      usersService.findOne.mockResolvedValueOnce(null);
      const result = await authService.validateUser(MAIL_DEFAULT, PASSWORD_DEFAULT);
      expect(result).toEqual(null);
    });
    it('should NOT validate user (wring password', async() => {
      const user: User= {id: "", mail: MAIL_DEFAULT, password: "wrong hash"};
      usersService.findOne.mockResolvedValueOnce(user);
      const result = await authService.validateUser(MAIL_DEFAULT, PASSWORD_DEFAULT);
      expect(result).toEqual(null);
    });


    it('should return an token', async()=>{
      const user: User= {id: "", mail: MAIL_DEFAULT, password: HASH_DEFAULT};
      const result = await authService.login(user);
      expect(result).toHaveProperty("access_token");
    })

    

    it('should return a new User', async()=>{
      const user: User= {id:"", mail: MAIL_DEFAULT, password: PASSWORD_DEFAULT};
      usersService.postUser.mockResolvedValueOnce(user);
      const result = await authService.postNewUser(user);
      expect(result).toHaveProperty("id");
      expect(result).toHaveProperty("mail");
      expect(result).toHaveProperty("password");
    })
  });