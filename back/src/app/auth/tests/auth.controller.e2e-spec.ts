import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../app.module';
import { DataSource, Repository } from 'typeorm';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { setupDB } from '../../../../test/setupDB';
import { MAIL_DEFAULT, PASSWORD_DEFAULT } from '../../../database/seed/seeds';
import { User } from '../../users/entities/user.entity';
import * as bcrypt from 'bcrypt';

//Give TypeOrm some time to run migrations
jest.setTimeout(10000000);

describe('AppController (e2e)', () => {
    let app: INestApplication;
    let userRepository: Repository<User>;

    beforeAll(async () => {
        const { db, dataSource } = await setupDB(true);
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRoot({
                    name: 'default',
                    synchronize: true,
                }),
                AppModule,
            ],
        })
            .overrideProvider(DataSource)
            .useValue(dataSource)
            .compile();

        app = moduleFixture.createNestApplication();

        //If you need to use a repository to test what's inside database:
        userRepository = app.get<Repository<User>>(getRepositoryToken(User));

        await app.init();
    });

    it('/auth/login (POST) return access token', async () => {
        const credentials = {
            mail: MAIL_DEFAULT,
            password: PASSWORD_DEFAULT,
        };
        const result = await request(app.getHttpServer()).post(`/auth/login`).send(credentials);
        expect(result.status).toEqual(200);
        expect(result.body).toHaveProperty('access_token');
    });

    it('/auth/login (POST) without mail return 401', async () => {
        const credentials = {
            mail: null,
            password: PASSWORD_DEFAULT,
        };
        const result = await request(app.getHttpServer()).post(`/auth/login`).send(credentials);
        expect(result.status).toEqual(401);
    });

    it('/auth/login (POST) without password return 401', async () => {
        const credentials = {
            mail: MAIL_DEFAULT,
            password: null,
        };
        const result = await request(app.getHttpServer()).post(`/auth/login`).send(credentials);
        expect(result.status).toEqual(401);
    });

    it('/auth/login (POST) with wrong credentials return 401', async () => {
        const credentials = {
            mail: 'wrong_email',
            password: 'wrong_password',
        };

        const result = await request(app.getHttpServer()).post(`/auth/login`).send(credentials);
        expect(result.status).toEqual(401);
    });

    it('/auth/login (POST) Without authentification fail', async () => {
        const user: User = { id: '', mail: 'myMail', password: 'password' };
        const result = await request(app.getHttpServer()).post(`/auth/user`).send(user);
        expect(result.status).toEqual(401);
    });
    it('/auth/user (POST) return new User', async () => {
        const user: User = { id: '', mail: 'myMail', password: 'password' };
        const credentials = {
            mail: MAIL_DEFAULT,
            password: PASSWORD_DEFAULT,
        };
        const authentication = await request(app.getHttpServer())
            .post(`/auth/login`)
            .send(credentials);
        const { access_token } = authentication.body;
        console.log(authentication);

        const result = await request(app.getHttpServer())
            .post(`/auth/user`)
            .set({ Authorization: `bearer ${access_token}` })
            .send(user);
        expect(result.status).toEqual(201);
        expect(result.body).toHaveProperty('id');

        //Is the new userId is the same in Database ?
        const userInDatabase: User = await userRepository.findOne({ where: { mail: user.mail } });
        expect(userInDatabase).toHaveProperty('id');
        expect(userInDatabase.id).toBe(result.body.id);

        //Will he be able to login ?
        const isMatch = await bcrypt.compare(user.password, userInDatabase.password);
        expect(isMatch).toBe(true);
    });

    it('/auth/user (POST) without mail return 401', async () => {
        const user: User = { id: '', mail: null, password: PASSWORD_DEFAULT };
        const { access_token } = (
            await request(app.getHttpServer()).get(
                `/auth/login?mail=${MAIL_DEFAULT}&password=${PASSWORD_DEFAULT}`,
            )
        ).body;

        const result = await request(app.getHttpServer())
            .post(`/auth/user`)
            .set({ Authorization: `bearer ${access_token}` })
            .send(user);
        expect(result.status).toEqual(401);
    });

    it('/auth/user (POST) without password return 401', async () => {
        const user: User = { id: '', mail: MAIL_DEFAULT, password: null };
        const { access_token } = (
            await request(app.getHttpServer()).get(
                `/auth/login?mail=${MAIL_DEFAULT}&password=${PASSWORD_DEFAULT}`,
            )
        ).body;

        const result = await request(app.getHttpServer())
            .post(`/auth/user`)
            .set({ Authorization: `bearer ${access_token}` })
            .send(user);
        expect(result.status).toEqual(401);
    });
});
