import { Test, TestingModule } from '@nestjs/testing';
import { HttpException } from '@nestjs/common';
import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { AuthService } from '../auth.service';
import { AuthController } from '../auth.controller';
import { HASH_DEFAULT, MAIL_DEFAULT, PASSWORD_DEFAULT } from '../../../database/seed/seeds';
import { User } from '../../users/entities/user.entity';

describe('authController', () => {
    let authController: AuthController;
    let authService: DeepMocked<AuthService>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AuthController],
        })
            .useMocker(createMock)
            .compile();

        authController = module.get<AuthController>(AuthController);
        authService = module.get(AuthService);
    });

    it('should be defined', () => {
        expect(authController).toBeDefined();
    });

    it('should login', async () => {
        const user: User = { id: '', mail: MAIL_DEFAULT, password: HASH_DEFAULT };
        authService.validateUser.mockResolvedValueOnce(user);
        authService.login.mockResolvedValueOnce({ access_token: 'myAccessToken' });
        const result = await authController.login(MAIL_DEFAULT, PASSWORD_DEFAULT);
        expect(result).toEqual({ access_token: 'myAccessToken' });
    });

    it('should throw Unauthorized (no mail)', async () => {
        await expect(authController.login(null, PASSWORD_DEFAULT)).rejects.toThrow(HttpException);
    });

    it('should throw Unauthorized (no password)', async () => {
        await expect(authController.login(MAIL_DEFAULT, null)).rejects.toThrow(HttpException);
    });

    it('should throw Unauthorized (mail/pwd missmatch))', async () => {
        authService.validateUser.mockResolvedValueOnce(null);
        await expect(authController.login(MAIL_DEFAULT, PASSWORD_DEFAULT)).rejects.toThrow(
            HttpException,
        );
    });

    it('POST should post user', async () => {
        const user: User = { id: '', mail: MAIL_DEFAULT, password: HASH_DEFAULT };
        authService.postNewUser.mockResolvedValueOnce(user);
        const result = await authController.postNewUser(user);
        expect(result).toEqual(user);
    });

    it('POST should throw Unauthorized (no mail)', async () => {
        const user: User = { id: '', mail: null, password: HASH_DEFAULT };
        await expect(authController.postNewUser(user)).rejects.toThrow(HttpException);
    });

    it('POST should throw Unauthorized (no password)', async () => {
        const user: User = { id: '', mail: MAIL_DEFAULT, password: null };
        await expect(authController.postNewUser(user)).rejects.toThrow(HttpException);
    });
});
