import { Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from '../users/entities/user.entity';
import { SALT_BCRYPT } from '../../database/seed/seeds';
import { v4 as uuid } from 'uuid';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService) {}

    async validateUser(mail: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(mail);
        if (!user) {
            return null;
        }
        const isMatch = await bcrypt.compare(pass, user?.password);

        if (isMatch) {
            const { password, ...result } = user;
            return result;
        }

        return null;
    }

    async postNewUser(newUser: Partial<User>): Promise<User> {
        newUser.password = await bcrypt.hash(newUser.password, SALT_BCRYPT);
        newUser.id = uuid();
        return await this.usersService.postUser(newUser);
    }

    async login(user: any) {
        const payload = { mail: user.mail, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
