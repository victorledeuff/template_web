
import { Test, TestingModule } from "@nestjs/testing";
import { createMock } from '@golevelup/ts-jest';
import { UsersService } from "../users.service";
import { UsersModule } from "../users.module";
import { Repository } from "typeorm";
import { User } from "../entities/user.entity";
import { getRepositoryToken } from "@nestjs/typeorm";



const mockedUser: User= {id:"", mail: "myMail", password: " "};
describe('UsersService', () => {
    let usersService: UsersService;
  let usersRepositoryMock: Repository<User>;
  
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [UsersService,
          { provide: getRepositoryToken(User), 
            useValue: {
              find: jest.fn().mockResolvedValue(mockedUser),
              findOne: jest.fn().mockResolvedValue(mockedUser),
              findOneOrFail: jest.fn().mockResolvedValue(mockedUser),
              create: jest.fn().mockReturnValue(mockedUser),
              save: jest.fn().mockReturnValue(mockedUser),
              // as these do not actually use their return values in our sample
              // we just make sure that their resolee is true to not crash
              update: jest.fn().mockResolvedValue(true),
              // as these do not actually use their return values in our sample
              // we just make sure that their resolee is true to not crash
              delete: jest.fn().mockResolvedValue(true),
            },  
          }],
      }).compile();

      usersService=module.get<UsersService>(UsersService);
  
    });
  
    it('should be defined', () => {
      expect(usersService).toBeDefined();
    });
    

    it('should return found User', async()=>{
      const result = await usersService.findOne(mockedUser.mail);
      expect(result.mail).toBe(mockedUser.mail);
    })
    
    it('should return new User', async()=>{
      const result = await usersService.postUser(mockedUser);
      expect(result).toBe(mockedUser);
    })
    
  });