import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
      ) {}

  async findOne(mail: string): Promise<User | undefined> {
    return this.userRepository.findOne({where:{ mail: mail}});
  }

  async postUser(user: Partial<User>): Promise<User>{
    return await this.userRepository.save(user);
  }
}