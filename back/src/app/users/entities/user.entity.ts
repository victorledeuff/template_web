
import { ApiProperty } from '@nestjs/swagger';
import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';

@Entity({  name: 'user' })
export class User {
    @ApiProperty({description:"Identifiant unique de l'utilisateur"})
    @PrimaryGeneratedColumn('uuid')
    id: string;

    
    @ApiProperty({description:"Adresse email de l'utilisateur, utilisé comme identifiant de connexion"})
    @Column({ type: 'varchar', length: 300, nullable: false, unique: true})
    mail: string;

    
    @ApiProperty({description:"Mot de passe de l'utilisateur (hash en base)"})
    @Column({ type: 'varchar', length: 300, nullable: false})
    password: string;
}