import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config/dist';
import { DatabaseModule } from '../database/database.module';
import * as Joi from '@hapi/joi';
import { HttpModule } from '@nestjs/axios'
import { AuthModule } from './auth/auth.module';
import { HealthModule } from './health/health.module';

require('dotenv').config();
@Module({
  imports: [
    HttpModule,
    ConfigModule.forRoot({
      envFilePath:".env",
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
      }),
    }),
    DatabaseModule,
    AuthModule,
    HealthModule,
  ],
})
export class AppModule {}
