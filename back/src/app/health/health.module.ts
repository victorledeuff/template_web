import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
/***
 * 
 * This module is use by docker (eventually sentry or Kubernetes) to monitor app 
 * 
 */
@Module({
    imports: [TerminusModule],
    controllers: [HealthController]
  })
export class HealthModule {}
