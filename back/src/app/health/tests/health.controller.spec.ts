import { createMock, DeepMocked } from '@golevelup/ts-jest';
import {
    HealthCheckResult,
    HealthCheckService,
    HealthIndicatorResult,
    TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Test, TestingModule } from '@nestjs/testing';
import { HealthController } from '../health.controller';

describe('HealthController', () => {
    let controller: HealthController;
    let health: DeepMocked<HealthCheckService>;
    let db: DeepMocked<TypeOrmHealthIndicator>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [HealthController],
        })
            .useMocker(createMock)
            .compile();

        controller = module.get<HealthController>(HealthController);
        health = module.get(HealthCheckService);
        db = module.get(TypeOrmHealthIndicator);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('should return healthy', async () => {
        const statusOk: HealthCheckResult = { status: 'ok', details: {} };
        health.check.mockResolvedValueOnce(statusOk);

        const dbOk: HealthIndicatorResult = { default: { status: 'up' } };
        db.pingCheck.mockResolvedValueOnce(dbOk);
        const result = await controller.check();
        expect(result).toEqual(statusOk);
    });
});
