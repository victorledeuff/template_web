import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../app.module';
import { DataSource } from 'typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import { setupDB } from '../../../../test/setupDB';

//Give TypeOrm some time to run migrations
jest.setTimeout(10000000);

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const { db, dataSource } = await setupDB(true);
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRoot({
                    name: 'default',
                    synchronize: true,
                }),
                AppModule,
            ],
        })
            .overrideProvider(DataSource)
            .useValue(dataSource)
            .compile();

        app = moduleFixture.createNestApplication();

        await app.init();
    });

    it('/health-check (GET) should return status OK', async () => {
        const result = await request(app.getHttpServer()).get(`/health-check`);
        expect(result.status).toEqual(200);
        expect(result.body).toHaveProperty('status');
        expect(result.body.status).toBe('ok');
    });
});
