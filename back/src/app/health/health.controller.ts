import { Controller, Get } from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";
import { HealthCheck, HealthCheckService, TypeOrmHealthIndicator } from "@nestjs/terminus";

/***
 * 
 * This controller is use by docker (eventually sentry or Kubernetes) to monitor app 
 * 
 */

@ApiTags("health-check")
@Controller('health-check')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private db: TypeOrmHealthIndicator,
  ) {}

  @ApiOperation({ summary: 'Route use to assure app is healthy (and database too)' })
  @Get()
  @HealthCheck()
  async check() {
    return this.health.check([() => this.db.pingCheck('default'),  ]);
  }
}