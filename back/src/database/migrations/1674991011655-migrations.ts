import { MigrationInterface, QueryRunner } from "typeorm";
import { PASSWORD_DEFAULT, MAIL_DEFAULT, SALT_BCRYPT, HASH_DEFAULT } from "../seed/seeds";



export class migrations1674991011655 implements MigrationInterface {
    name = 'migrations1674991011655'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "mail" character varying(300) NOT NULL, "password" character varying(300) NOT NULL, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        
        await queryRunner.query(`insert into "user" ("mail", "password") values ('${MAIL_DEFAULT}', '${HASH_DEFAULT}')`);
            
    
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
     }

}
